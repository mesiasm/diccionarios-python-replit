"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 26:
             Dada una lista de países y ciudades de cada país, luego los nombres de las
             ciudades. Para cada ciudad, imprima el país en el que se encuentra."""
# Read a string:
# s = input()
# Print a value:
# print(s)
n = int(input())
diccionario ={}
for i in range (n):
    a = list(input().split())
    diccionario[a[0]] = a[1:]
m = int(input())
for t in range (m):
    b = input()
    for pais, ciudad in diccionario.items():
        if b in ciudad:
            print(pais)