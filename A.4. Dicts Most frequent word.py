"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 24:
            Dado el texto: la primera línea contiene el número de líneas, luego
          las líneas de palabras. Imprima la palabra en el texto que ocurre con
          más frecuencia. Si hay muchas de esas palabras, imprima la que sea menor
          en orden alfabético."""
# Read a string:
# s = input()
# Print a value:
# print(s)
cont_palabra = {}
for i in range(int(input())):
  palabras = input().split()
  for palabra in palabras:
    if palabra not in cont_palabra:
      cont_palabra[palabra] = 0
    cont_palabra[palabra] += 1
max_frecuencia = max(cont_palabra.values())
for palabra in sorted(cont_palabra):
  if cont_palabra[palabra] == max_frecuencia:
    print(palabra)
    break