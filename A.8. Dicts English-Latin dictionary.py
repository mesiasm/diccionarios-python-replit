"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 28:
             Ayúdale a crear un latín-inglés.
             La primera línea contiene un solo número entero N , el número de palabras en inglés
             en el diccionario, seguido de N entradas de diccionario. Cada entrada está contenida
             en una línea separada, que contiene primero la palabra en inglés, luego un guión rodeado
             de espacios y luego una lista separada por comas con las traducciones de esta palabra en
             inglés en latín. Todas las palabras consisten solo en letras minúsculas en inglés. Las
             traducciones están ordenadas en orden lexicográfico. El orden de las palabras en inglés
             en el diccionario también es lexicográfico.
             Imprima el diccionario latino-inglés correspondiente en el mismo formato. En particular, la
             primera línea de la palabra debe ser la traducción lexicográficamente mínima de la palabra
             latina, luego la segunda en ese orden, etc. Las palabras en inglés dentro de cada línea también
             deben clasificarse lexicográficamente."""
# Read a string:
# s = input()
# Print a value:
# print(s)
dicionario = {}
for _ in range(int(input())):
  ingre, *latin = input().replace('- ', '').replace(',', '').split()
  for palabra in latin:
    if palabra not in dicionario.keys():
      dicionario[palabra] = []
    dicionario[palabra].append(ingre)

print()
print(len(dicionario))
for i in sorted(dicionario.keys()):
  print(i, '-', ', '.join(sorted(list(dicionario[i]))))
