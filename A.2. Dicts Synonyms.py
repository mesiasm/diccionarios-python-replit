"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 22:
            Te dan un diccionario que consiste en pares de palabras. Cada palabra es
            sinónimo de la otra palabra en su par. Todas las palabras en el
            diccionario son diferentes.
            Después del diccionario hay una palabra más dada. Encuentra un sinónimo para ello."""
# Read a string:
# s = input()
# Print a value:
# print(s)
num = int(input())
dicionario = {}
palabras = list(input().split())
for i in range (num):
    dicionario[palabras[0]] = palabras[1]
    if i < num-1:
        palabras = list(input().split())
palabra = input()
for j , h in dicionario.items():
    if j == palabra:
        print(h)
    if h == palabra:
        print(j)
"""
OTRA FORMA DE RESOLVER:
sinonimo = {}
for i in range(int(input())):
  letra_uno, letra_dos = input().split()
  sinonimo[letra_uno] = letra_dos
  sinonimo[letra_dos] = letra_uno
print(sinonimo[input()])
"""